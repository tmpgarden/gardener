FROM node:alpine

RUN apk add -U git
RUN npm install --global bower gulp-cli
RUN mkdir -p /usr/src/app/public
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

COPY public/package.json /usr/src/app/public/
COPY public/bower.json /usr/src/app/public/
RUN cd public && npm install && bower install --allow-root
COPY . /usr/src/app
RUN cd public && gulp inject

EXPOSE 3000

CMD [ "npm", "start" ]
