var express = require('express');
var bodyParser = require('body-parser');
var APIDB = require('./api/db');
var _ = require('lodash');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.use('/', express.static('public'));
app.use('/api',bodyParser.urlencoded({ extended: false }));
app.use('/api', bodyParser.json());

var API = {
  framework: require(__dirname + '/lib/framework'),
  controllers: require(__dirname + '/api/controllers'),
  services: require(__dirname + '/api/services')
};

API.socket = io;

API.models = new APIDB( __dirname + '/gardener.sqlite', function(err){
  if(err) {
    console.log(err);
  } else {
    app.use('/api', API.framework.generators.routes());
    API.services.flower.init(process.env.KUE);
    http.listen(process.env.PORT || 3000, function () {
      console.log('Listening on port ' + (process.env.PORT || 3000));
    });
  }
});

GLOBAL.API = API;
