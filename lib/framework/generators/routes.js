var _ = require('lodash');
var express = require('express');
var router = express.Router();

function generator(){
  _.each(API.controllers, function(controller, key){
    router.use( '/' + key,
      generateRoutes(API.models[key], controller)
    );
  });

  return router;
}

function generateRoutes(model, controller){
  // new Router for this model-controller
  var router = express.Router();
  // "extend" from base CRUD controller
  if(model){
    _.defaults(controller, require(__dirname + '/../base/controller'));
    // Inject model in request
    router.all('*',function(req, res, next){
      req.model = model;
      next();
    });
  }
  // generate CRUD + custom routes
  _.each(controller, function(fn, key){
    var method, route;
    switch (key) {
      case 'all':
        method = 'get';
        route = '/';
        break;
      case 'one':
        method = 'get';
        route = '/:id';
        break;
      case 'create':
        method = 'post';
        route = '/';
        break;
      case 'update':
        method = 'put';
        route = '/:id';
        break;
      case 'destroy':
        method = 'delete';
        route = '/:id';
        break;
      default:
        var map = key.split(' ');
        method = map[0];
        route = map[1];
        break;
    }

    router[method](route, fn);
  });

  return router;
}

module.exports = generator;
