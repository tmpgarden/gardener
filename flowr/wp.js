module.exports = {
	title: "WordPress Service",
	serviceId: "wp",
	description: "Some WordPress relates methods",
	methods: {
		getById: {
			title: "Get Post by ID",
			description: "Get Post data",
			method: "getById",
			params: {
				postId: {
					type: 'int',
					description: 'WP Post id',
					example: 8883
				},
				wp: {
					type: 'config',
					description: "WP Cient configuration",
					example: {
						url: "http://mywordpress.org",
						username: "XMLRPC_username",
						password:"XMLRPC_password"
					}
				}
			},
			output: {
				type: 'object',
				description: 'An object containig WordPress Post data',
				example: {
					title: 'Teknomadak'
				}
			},
		},
		post: {
			title: "Create New Post",
			description: "Create new post",
			method: "post",
			params: {
				post: {
					type: 'object',
					description: 'WP Post Object',
					example: '{post_type: "post",status: "publish",title: "Title",author: 1,excerpt: "Post excerpt",content: "Post content", date: "2015-04-19T19:26:30.876Z",terms: {"category":[1]},termNames: {"post_tag":["some","tags"]},thumbnail: 1, commentStatus: "open",customFields: [{key:"enclosure",value:"http://your.audio/\r\n9999\r\naudio/ogg\r\na:1:{s:8:\"duration\";s:8:\"01:30:00\"}"} ]}'
				},
				wp: {
					type: 'config',
					description: "WP Cient configuration",
					example: {
						url: "http://mywordpress.org",
						username: "XMLRPC_username",
						password:"XMLRPC_password"
					}
				}
			},
			output: {
				type: 'object',
				description: 'An object containig WordPress Post data',
				example: {
					title: 'Teknomadak'
				}
			},
		}
	}


}
