module.exports = {
	title: "Transcode Service",
	serviceId: "transcode",
	description: "FFMPG related methods",
	methods: {
		audio: {
			title: "Transcode audio",
			description: "Transcode audio",
			method: "audio",
			params: {
				file: {
					type: 'url',
					description: 'File to transcode',
					example: "http://myexchange/song.mp3"
				},
				format:{
					type: 'string',
					description: 'Format to transcode to',
					example: 'ogg'
				},
				quality: {
					type: 'number',
					description: "Quality [0-10] to transform to. Has preference over `bitrate`",
					example: 5
				},
				bitrate: {
					type: 'number',
					description: "Bitrate to transform to. `quality` has preference",
					example: 320
				}
			},
			output: {
				type: 'url',
				description: 'Url path to transcoded audio',
				example: {
					title: 'http://exchange.com/35e38d34'
				}
			},
		}
	}
}
