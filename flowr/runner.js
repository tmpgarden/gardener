module.exports = {
	title: 'Flow runner sevice',
	serviceId: 'runner',
	description: 'Run block flows',
	methods: {
		serial: {
			title: 'Execute serial flow',
			description: 'Run every block in a flow',
			method: 'serial',
			params: {
				jobs: {
					type: 'jobs',
					description: 'An array containing jobs',
					example: 'TODO'
				},
				input: {
					type: 'multiple',
					description: 'The initial input for the first block in flow.',
					example: 'TODO'
				}
			},
			output: {
				type: 'multiple',
				description: 'The result of the last block in flow.',
				example: {
					todo: 'TODO'
				}
			}
		},
		parallel: {
			title: 'Execute parallel flow',
			description: 'Run in parallel every block in a flow',
			method: 'parallel',
			params: {
				jobs: {
					type: 'jobs',
					description: 'An array containing jobs',
					example: 'TODO'
				},
				input: {
					type: 'multiple',
					description: 'The initial input for the first block in flow.',
					example: 'TODO'
				}
			},
			output: {
				type: 'multiple',
				description: 'The result of the last block in flow.',
				example: {
					todo: 'TODO'
				}
			}
		}
	}
};
