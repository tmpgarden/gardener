module.exports = {
  flower: require('./runner'),
  vm: require('./ops'),
  cloud: require('./cloud-upload'),
  recstore: require('./recstore'),
  repeater: require('./repeater'),
  transcode: require('./transcode'),
  wp: require('./wp')
};
