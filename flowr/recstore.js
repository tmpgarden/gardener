module.exports = {
	title: 'Repeater service',
	serviceId: 'recstore',
	description: 'Recordings store service',
	methods: {
		add: {
			title: 'Add',
			description: 'Add new recording to collection',
			method: 'add',
			params: {
				url: {
					type: 'string',
					description: 'URL of media file to push',
					example: 'http://ttanttakun.org/api/irratsaioak'
				},
				irratsaioa: {
					type: 'string',
					description: 'name of collection',
					example: 'TEKNOMADAK'
				},
				filename: {
					type: 'string',
					description: 'name of the file',
					example: 'TEKNOMADAK.mp3'
				}
			},
			output: {
				type: 'boolean',
				description: 'The result of the push',
				example: true
			}
		}
	}
}
