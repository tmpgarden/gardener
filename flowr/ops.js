module.exports = {
	title: "VM Service",
	serviceId: "ops",
	description: "Execute javascript operations",
	methods: {
		transform: {
			title: "Transform",
			description: "execute custom transform operation and return arbitrary data",
			method: "transform",
			params: {
				transform: {
					type: 'function',
					description: 'custom transform function (stringified...)',
					example: "function transform(input){\n return input.count +1\n}"
				},
				input: {
					type: 'multiple',
					description: "data to transform",
					example: '{\ncount: 5\n}'
				}
			},
			output: {
				type: 'multiple',
				description: 'transform funcion return',
				example: 6
			},
		}
	}
};
