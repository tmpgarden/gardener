module.exports = {
	title: "Cloud Upload Service",
	serviceId: "cloud",
	description: "Cloud related methods",
	methods: {
		upload: {
			title: "Upload",
			description: "Upload file to cloud provider",
			method: "upload",
			params: {
				srcPath: {
					type: 'url',
					description: 'local path file to upload',
					example: "/Music/myexchange/song.mp3"
				},
				destPath:{
					type: 'url',
					description: 'Full destination for cloud object storage.',
					example: 'uploads/song.mp3'
				},
				cloud: {
					type: 'config',
					description: "Cloud Cient configuration",
					example: {
						authUrl: "http://myauthurl.org",
						username: "cloud_username",
						password:"cloud_password",
						version: "v2.0",
						provider:"openstack",
						region:"GRA1",
						container:"mycontainer"
					}
				}
			},
			output: {
				type: 'object',
				description: 'An object containig WordPress Post data',
				example: {
					title: 'Teknomadak'
				}
			},
		}
	}
}
