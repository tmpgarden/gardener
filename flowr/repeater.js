module.exports = {
	title: "Repeater service",
	serviceId: "repeater",
	description: "Repeater service",
	methods: {
		push: {
			title: "Push",
			description: "Push audio to repeat stack",
			method: "push",
			params: {
				item: {
					type: 'string',
					description: 'URL of media file to push',
					example: "http://ttanttakun.org/api/irratsaioak"
				},
				collection: {
					type: 'string',
					description: 'name of collection',
					example: "TEKNOMADAK"
				}
			},
			output: {
				type: 'boolean',
				description: 'The result of the push',
				example: true
			},
		}
	}
}
