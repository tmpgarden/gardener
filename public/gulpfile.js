var gulp = require('gulp');
var wiredep = require('wiredep').stream;

gulp.task('inject', function () {
  gulp.src('./app/index.html')
    .pipe(wiredep())
    .pipe(gulp.dest('./'));
});