angular.module('ttn.models.block', [])

.service('BlockModel', function($q, $http) {
    this.getAll = function() {
        var deferred = $q.defer();

        $http.get("/api/block").then(function(models) {
            return deferred.resolve(models);
        });

        return deferred.promise;
    };

    this.getOne = function(id) {
        var deferred = $q.defer();

        $http.get("/api/block/"+id).then(function(model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    };

    this.create = function(newModel) {
        var deferred = $q.defer();

        $http.post("/api/block", newModel).then(function(model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    };

    this.edit = function(block) {
        var deferred = $q.defer();

        $http.put("/api/block/"+block.id, block).then(function(model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    };
});
