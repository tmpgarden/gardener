angular.module('ttn.models.flow', [])

.service('FlowModel', function($q, $http) {

    this.getAll = function() {
        var deferred = $q.defer();

        $http.get("/api/flow").then(function(models) {
            return deferred.resolve(models.data);
        });

        return deferred.promise;
    };

    this.getOne = function(id) {
        var deferred = $q.defer();

        $http.get("/api/flow/"+id).then(function(model) {
          console.log(model);
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.create = function(newModel) {
        var deferred = $q.defer();

        $http.post("/api/flow", newModel).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.edit = function(flow) {
        console.log(flow);
        var deferred = $q.defer();

        $http.put("/api/flow/"+flow.id, flow).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.delete = function(flow) {

        var deferred = $q.defer();

        $http.delete("/api/flow/"+flow.id).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.run = function(flowId, input) {
        console.log("RUNNING FLOW: ",flowId);
        var deferred = $q.defer();

        $http.post("/api/flow/" + flowId + "/run", input).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };
});
