angular.module('ttn.models.hook', [])

.service('HookModel', function($q, $http) {

    this.getAll = function() {
        var deferred = $q.defer();

        $http.get("/api/hook").then(function(models) {
            return deferred.resolve(models.data);
        });

        return deferred.promise;
    };

    this.getOne = function(id) {
        var deferred = $q.defer();

        $http.get("/api/hook/"+id).then(function(model) {
          console.log(model);
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.create = function(newModel) {
        var deferred = $q.defer();

        $http.post("/api/hook", newModel).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.edit = function(model) {

        var deferred = $q.defer();

        $http.put("/api/flow/" + model.id, model).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

    this.delete = function(model) {

        var deferred = $q.defer();

        $http.delete("/api/hook/" + model.id).then(function(model) {
            return deferred.resolve(model.data);
        });

        return deferred.promise;
    };

});
