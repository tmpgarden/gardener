angular.module( 'ttn.about', [
])

.config(function config( $stateProvider ) {
	$stateProvider.state( 'about', {
		url: '/about',
		views: {
			"main": {
				controller: 'AboutCtrl',
				templateUrl: 'app/about/index.tpl.html'
			}
		}
	});
})

.controller( 'AboutCtrl', function AboutController( $scope) {
	console.log("yep")
});