angular.module( 'ttn.header', [
])

.controller( 'HeaderCtrl', function HeaderController( $scope, $state, user, appInfo ) {
  $scope.user = user;
  $scope.app = appInfo;
});