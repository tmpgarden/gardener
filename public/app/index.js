var ttnApp = angular.module( 'ttn', [
    'ui.router',
    'ttn.header',
    'ttn.about',
    'ttn.login',
    'ttn.flow',
    'ttn.nflow',
    'ttn.hook',
    'ttn.models.block',
    'ttn.models.flow',
    'ttn.models.hook',
    'ui.ace',
    'btford.socket-io'
]);

ttnApp.constant('appInfo', {
	name: "TTN FlowR"
})
.constant('user', {})
.config( function myAppConfig ( $stateProvider, $urlRouterProvider, $locationProvider, user ) {

	var route = "/login";

	if(user){
		route = "/about";
	}

    $urlRouterProvider.otherwise( route );
})
.factory('socket', function (socketFactory) {
  return socketFactory();
});

angular.element(document).ready(
    function() {
        angular.bootstrap(document, ['ttn']);
        // var initInjector = angular.injector(['ng']);
        // var $http = initInjector.get('$http');
        // $http.get('/user/me').success(
        //     function (data) {
        //        var user = data;
        //        ttnApp.constant('user', user);
        //        angular.bootstrap(document, ['ttn']);
        //     }
        // );
    }
);
