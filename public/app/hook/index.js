angular.module( 'ttn.hook', [
])

.config(function config( $stateProvider ) {
	$stateProvider.state( 'hooks', {
		url: '/hooks',
		views: {
			"main": {
				controller: 'HookCtrl',
				templateUrl: 'app/hook/index.tpl.html'
			}
		}
	});
})

.controller( 'HookCtrl', [ '$scope', 'HookModel', 'FlowModel', '$window', function HookController( $scope, HookModel, FlowModel, $window) {

	$scope.newHook = {};

	HookModel.getAll().then(function(hooks){
		$scope.hooks = hooks;
	});

	FlowModel.getAll().then(function(flows){
		$scope.flows = flows;
	});


	$scope.createHook = function(){
		console.log($scope.newHook);
		HookModel.create($scope.newHook).then(function(model){
			$scope.newHook = {};
			$scope.hooks.push(model);
		});
	};

	$scope.deleteHook = function(hook){
		if($window.confirm("ARE YOU SURE?")) {
			HookModel.delete(hook).then(function(){
				$scope.hooks.splice($scope.hooks.indexOf(hook),1);
			});
		}
	};


}]);
