angular.module( 'ttn.flow', [
])

.config(function config( $stateProvider ) {
	$stateProvider.state( 'flows', {
		url: '/flows',
		views: {
			"main": {
				controller: 'FlowListCtrl',
				templateUrl: 'app/flow/list.tpl.html'
			}
		}
	})
	.state( 'edit', {
		url: '/edit/:id',
		views: {
			"main": {
				controller: 'FlowEditCtrl',
				templateUrl: 'app/flow/edit.tpl.html',
				resolve: {
          flow: ['$q','FlowModel','$stateParams', function($q, FlowModel, $stateParams){
            var deferred = $q.defer();
						if($stateParams.id) {

	            FlowModel.getOne($stateParams.id).then(function(data){
								console.log(data);
	              deferred.resolve(data);
	            });
						} else {
							deferred.resolve(null);
						}
            return deferred.promise;
          }],
        }
			}
		}
	});
})
.controller( 'FlowListCtrl', function FlowListController($scope, $http, BlockModel, FlowModel, $window) {

	FlowModel.getAll().then(function(flows){

			$scope.flows = flows;
	});



	$scope.deleteFlow = function(flow) {
		var confirm = $window.confirm('are you sure?');
		if(confirm) {
			FlowModel.delete(flow).then(function(deleted){
				console.log(deleted);
				$scope.flows.splice($scope.flows.indexOf(flow),1);
			});
		}

	};



})

.controller( 'FlowEditCtrl', function FlowEditController(
	$scope,
	$http,
	BlockModel,
	FlowModel,
	flow,
	$window,
	socket) {

	var flowCount = 0;
	var blockCount = 0;

	$scope.flows = {};
	$scope.editorData = {};



	FlowModel.getAll().then(function(flows){
			$scope.savedFlows = flows;
	});




	$scope.loadFlow = function(flow) {
		if(!$scope.flows[flow.id]) flowCount++;
		$scope.flows[flow.id] = flow;
		$scope.activateFlow(flow.id);
		$scope.editorData[flow.id] = undefined;
	};

	$scope.loadFlowFromId = function(id) {
		FlowModel.getOne(id).then(function(flow){
			$scope.loadFlow(flow);
		});
	};

	$scope.createNewFlow = function(cb){

		var newFlow = {
			title:"flow #"+flowCount,
			blocks: []
		};

		FlowModel.create(newFlow).then(function(flow){
			flowCount++;
			$scope.flows[flow.id] = flow;
			$scope.activateFlow(flow.id);
			$scope.editorData[flow.id] = undefined;
			console.log($scope.flows);
			if(cb) cb(flow.id);
		});

	};

	$http.get('/api/service').success(function(result){
		$scope.services = result;
	});

	$scope.addBlock = function(serviceId, method){

		var params = {};
		angular.forEach(method.params, function(val,key){
			params[key] = angular.copy(val.example);
		});

		var block = {
			title: "Block #" + blockCount++,
			service: serviceId,
			method: method.method,
			info: {
				params: method.params,
				output: method.output
			},
			params: params,
			input: {},
			inputOrder: {}
		};

		//console.log($scope.activeFlow);

		if(!$scope.flows[$scope.activeFlow].blocks) $scope.flows[$scope.activeFlow].blocks = [];
		$scope.flows[$scope.activeFlow].blocks.push(block);
		//console.log(flows);

	};

	$scope.removeBlock = function(index) {
		if($window.confirm("Are you sure?")){
			$scope.flows[$scope.activeFlow].blocks.splice(index,1);
		}
	};

	$scope.up = function(index){
		var tmp = $scope.flows[$scope.activeFlow].blocks[index-1];
		$scope.flows[$scope.activeFlow].blocks[index-1] = $scope.flows[$scope.activeFlow].blocks[index];
		$scope.flows[$scope.activeFlow].blocks[index] = tmp;
	};

	$scope.down = function(index){
		var tmp = $scope.flows[$scope.activeFlow].blocks[index+1];
		$scope.flows[$scope.activeFlow].blocks[index+1] = $scope.flows[$scope.activeFlow].blocks[index];
		$scope.flows[$scope.activeFlow].blocks[index] = tmp;
	};

	$scope.editBlock = function(index){
		var block = angular.copy($scope.flows[$scope.activeFlow].blocks[index]);
		$scope.editorData[$scope.activeFlow] = block;
		$scope.editorData[$scope.activeFlow].jobIndex = index;
	};

	$scope.saveBlock = function(){
		var jobIndex = $scope.editorData[$scope.activeFlow].jobIndex;
		delete $scope.editorData[$scope.activeFlow].jobIndex;

		var job = $scope.editorData[$scope.activeFlow];

		if (! job.id ) {

			BlockModel.create(job).then(function(savedBlock) {
				console.log(savedBlock);
				$scope.flows[$scope.activeFlow].blocks[jobIndex] = savedBlock.data;
				$scope.editorData[$scope.activeFlow] = undefined;
			});
		} else {
			BlockModel.edit(job).then(function(savedBlock) {
				console.log(savedBlock);
				$scope.flows[$scope.activeFlow].blocks[jobIndex] = savedBlock.data;
				$scope.editorData[$scope.activeFlow] = undefined;
			});
		}

		// $scope.flows[$scope.activeFlow].blocks[jobIndex] = angular.copy($scope.editorData[$scope.activeFlow])
		// $scope.editorData[$scope.activeFlow] = undefined
	};

	$scope.cancelBlock = function(){
		$scope.editorData[$scope.activeFlow] = undefined;
	};

	$scope.activateFlow = function(id){
		$scope.activeFlow = id;
	};

	$scope.attachNewFlow = function(flowId){
		$scope.createNewFlow(function(id){
			console.log(id);
			console.log($scope.editorData[flowId].params.jobs);
			if(!Array.isArray($scope.editorData[flowId].params.jobs)) $scope.editorData[flowId].params.jobs = [];
			$scope.editorData[flowId].params.jobs.push(id);
			console.log($scope.editorData[flowId]);
		});
	};

	$scope.attachSavedFlowFromId = function(id){
		FlowModel.getOne(id).then(function(flow){
			if(!Array.isArray($scope.editorData[$scope.activeFlow].params.jobs)) $scope.editorData[$scope.activeFlow].params.jobs = [];
			$scope.editorData[$scope.activeFlow].params.jobs.push(id);

			$scope.cancelSavedFlowsModal();
			$scope.loadFlow(flow);
		});

	};

	$scope.dettachFlow = function(index){
		$scope.editorData[$scope.activeFlow].params.jobs.splice(index,1);
	};


	$scope.showSavedFlowsModal = function(flowId){
		$scope.showModal = true;
	};

	$scope.cancelSavedFlowsModal = function(){
		$scope.savedFlowToLoad = null;
		$scope.showModal = false;
	};

	$scope.saveFlow = function(){
		saveFlow(angular.copy($scope.flows[$scope.activeFlow]), function(savedFlow){
			console.log(savedFlow);
			$scope.flows[$scope.activeFlow].id = savedFlow.id;
			console.log($scope.flows[$scope.activeFlow]);
		});
	};

	 $scope.runFlow = {};

	$scope.runActiveFlow = function(){
		console.log($scope.activeFlow);
		console.log($scope.runFlow.json);
		console.log($scope.runFlow.string);
		var input = $scope.runFlow.json ? JSON.parse($scope.runFlow.json) : $scope.runFlow.string;
		console.log(input);
		FlowModel.run($scope.activeFlow, input).then(function(res){
			console.log(res);
		});
	};

	socket.on('result', function(data){
		console.log(data);
	});


	function saveFlow(ngflow, cb) {
		var jobs = [];
		var order = 0;
		angular.forEach(ngflow.blocks, function(val, key){
			jobs.push(val.id);
		});

		console.log(jobs);
		ngflow.blocks = jobs;

		console.log(ngflow);

		FlowModel.edit(ngflow).then(function(savedFlow){
			console.log(savedFlow);
			cb(savedFlow);
		});
	}




	if(flow){
		$scope.loadFlow(flow);
	} else {
		$scope.createNewFlow();
	}




	$scope.getFlowTitle = function(id){
		var title = '';
		angular.forEach($scope.savedFlows, function(val, key){
				if(val.id == id) {
					title = val.title;
				}
		});
		return title;
	};




});














function parseFlow(ngflow) {
	console.log("PARSING FLOW");
	console.log(ngflow);

	var jobs = [];

	angular.forEach(ngflow.blocks, function(val, key){
		var job = {
			id: val.id,
			title: ngflow.title+": "+val.title,
			service: val.service,
			method: val.method,
			params: val.params
		};
		var params = {};

		var inputs = 0;

		angular.forEach(val.input, function(input, name){
			console.log(input);
			if(input){
				inputs++;
			}
		});


		angular.forEach(val.params, function(param,name){
			if(val.input[name]) {
				if(val.inputOrder[name]>= 0 && inputs>1){
					params[name] = '<INPUT['+val.inputOrder[name]+']>';
				} else {
					params[name] = '<INPUT>';
				}
			} else {
				params[name] = param;
			}
		});

		job.params = params;
		jobs.push(job);
	});

	var flow = {
		id: ngflow.id,
		title: ngflow.title,
		blocks: jobs
	};

	console.log(flow);

	return flow;
}
