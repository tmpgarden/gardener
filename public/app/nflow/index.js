angular.module( 'ttn.nflow', [
	'ttn.jsplumb'
])
.config(function config( $stateProvider ) {
	$stateProvider
  .state( 'nflow', {
		url: '/nflow',
		views: {
			"fluid": {
				controller: 'NFlowEditorCtrl',
				templateUrl: 'app/nflow/editor.tpl.html'
			}
		}
	});
})
.controller( 'NFlowEditorCtrl', function NFlowEditorController(
  $scope,
  $http,
  BlockModel,
  FlowModel,
  $window,
	jsplumb
) {


	var items = [
		{name:"Window1"},
		{name:"Window2"},
		{name:"Window3"},
		{name:"Window4"},
	];


	jsplumb.init({
		name: "Test",
		container: "canvas",
		items: items
	});




});
