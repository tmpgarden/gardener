function jsPlumbService(){

  var connectorPaintStyle = {
    lineWidth: 2,
    strokeStyle: "#61B7CF",
    joinstyle: "round",
    outlineColor: "white",
    outlineWidth: 2
  };
  var connectorHoverStyle = {
    lineWidth: 2,
    strokeStyle: "#216477",
    outlineWidth: 2,
    outlineColor: "white"
  };
  var endpointHoverStyle = {
    fillStyle: "#216477",
    strokeStyle: "#216477"
  };
  var sourceEndpoint = {
    endpoint: "Dot",
    paintStyle: {
      strokeStyle: "#7AB02C",
      fillStyle: "transparent",
      radius: 7,
      lineWidth: 3
    },
    isSource: true,
    connector: [ "Flowchart", { stub: [40, 60], gap: 10, cornerRadius: 5, alwaysRespectStubs: false } ],
    connectorStyle: connectorPaintStyle,
    hoverPaintStyle: endpointHoverStyle,
    connectorHoverStyle: connectorHoverStyle,
    dragOptions: {},
    overlays: [
      [ "Label", {
        location: [0.5, 1.5],
        label: "Drag",
        cssClass: "endpointSourceLabel",
        visible:false
      } ]
    ]
  };
  var targetEndpoint = {
    endpoint: "Dot",
    paintStyle: { fillStyle: "#7AB02C", radius: 7 },
    hoverPaintStyle: endpointHoverStyle,
    maxConnections: 1,
    dropOptions: { hoverClass: "hover", activeClass: "active" },
    isTarget: true,
    overlays: [
      [ "Label", { location: [0.5, -0.5], label: "Drop", cssClass: "endpointTargetLabel", visible:false } ]
    ]
  };

  var connectionOverlays = [
    [ "Arrow", {
      location: 1,
      visible:true,
      id:"ARROW",
      events:{
        click: function() {
          alert("you clicked on the arrow overlay");
        }
      }
    } ],
    [ "Label", {
      location: 0.1,
      id: "label",
      cssClass: "aLabel",
      events:{
        tap: function() {
          alert("hey");
        }
      }
    }]
  ];

  function init(connection) {
      connection.getOverlay("label").setLabel(connection.sourceId + "-" + connection.targetId);
  }

  function addItem(item) {
    var instance = this;
    var sourceAnchors = ["BottomCenter"];
    var targetAnchors = ["TopLeft", "TopRight"];
    for (var i = 0; i < sourceAnchors.length; i++) {
      var sourceUUID = item.name + sourceAnchors[i];
      instance.addEndpoint("flowchart" + item.name, sourceEndpoint, {
          anchor: sourceAnchors[i], uuid: sourceUUID
      });
    }
    for (var j = 0; j < targetAnchors.length; j++) {
      var targetUUID = item.name + targetAnchors[j];
      instance.addEndpoint("flowchart" + item.name, targetEndpoint, {
        anchor: targetAnchors[j],
        uuid: targetUUID
      });
    }
  }

  var instances = {};

  var jsplumb = {};

  function addBatch(items){
    var instance = this;

    instance.batch(function () {
      for (var i = 0; i < items.length; i++) {
        instance.addItem(items[i]);
      }
    });
  }

  jsplumb.init = function(args){

    if(instances[args.name]) {
      console.log("CACHED");
      return instances[args.name];
    }

    console.log("NEW");

    var instance = jsPlumb.getInstance({
      DragOptions: { cursor: 'pointer', zIndex: 2000 },
      ConnectionOverlays: connectionOverlays,
      Container: args.container || 'canvas'
    });

    instance.addItem = addItem.bind(instance);

    instance.addBatch = addBatch.bind(instance);

    if(args.items) {
      instance.addBatch(args.items);
    }

    instance.bind("connection", function (connInfo, originalEvent) {
        init(connInfo.connection);
    });

    instance.draggable(jsPlumb.getSelector(".flowchart-demo .window"), { grid: [20, 20] });

    instances[args.name] = instance;

    return instance;
  };










      // // suspend drawing and initialise.
      // instance.batch(function () {
      //
      //   _addEndpoints("Window4", , ["TopLeft", "TopRight"]);
      //   _addEndpoints("Window2", ["BottomCenter"], );
      //   _addEndpoints("Window3", ["BottomCenter"], ["TopLeft", "TopRight"]);
      //   _addEndpoints("Window1", ["BottomCenter"], ["TopLeft", "TopRight"]);
      //
      //   // make all the window divs draggable
      //   instance.draggable(jsPlumb.getSelector(".flowchart-demo .window"), { grid: [20, 20] });
      //   // THIS DEMO ONLY USES getSelector FOR CONVENIENCE. Use your library's appropriate selector
      //   // method, or document.querySelectorAll:
      //   //jsPlumb.draggable(document.querySelectorAll(".window"), { grid: [20, 20] });
      //
      //   // connect a few up
      //   instance.connect({uuids: ["Window2TopRight", "Window3TopCenter"], editable: true});
      //
      //   //
      //   // listen for clicks on connections, and offer to delete connections on click.
      //   //
      //   instance.bind("click", function (conn, originalEvent) {
      //      // if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?"))
      //        //   instance.detach(conn);
      //       conn.toggleType("basic");
      //   });
      // });


  return jsplumb;
}

angular.module( 'ttn.jsplumb', [
])
.service('jsplumb',jsPlumbService);
