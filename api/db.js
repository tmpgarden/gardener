var Sequelize = require('sequelize');

function DB(dbPath, cb){
  var sequelize = new Sequelize('gardener', process.env.DB_USER || 'root', process.env.DB_PASS || 'root', {
    dialect: 'mysql',
    host: process.env.DB_HOST || 'localhost',
    logging: false
  });

  // Models
  this.flow = sequelize.import(__dirname + "/models/flow.js");
  this.block = sequelize.import(__dirname + "/models/block.js");
  this.flowBlock = sequelize.define('FlowBlock', {
    seq: 'integer'
  });

  // Associations
  this.flow.belongsToMany(this.block, {through: this.flowBlock});
  this.block.belongsToMany(this.flow, {through: this.flowBlock});

  sequelize.sync().then(function() {
    console.log("DB sync'ed. Let's go!");
    cb();
  }).catch(function(error) {
    console.log("ERROR", error);
    cb(error);
  });
}

module.exports = DB;
