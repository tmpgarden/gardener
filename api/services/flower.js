var Hose = require('flowr-hose');
var _ = require('lodash');
var hose;


module.exports = {

	run: function(flowId, input, cb, jobCallback){
		API.models.flow
    .findById(flowId, {
      include: [ { model: API.models.block } ],
      order: [ [ API.models.block, API.models.flowBlock, 'seq' ] ]
    }).then(function(flow){
			parseFlow(flow, function(parsedFlow){
				var job = hose.create('runner:serial', {
				    title: parsedFlow.title,
				    params: {
				    	jobs: parsedFlow.blocks,
				    	input: input
				    },
				}, function(err, result){
					if(err){
						console.error("FLOW ERROR: ", err);
						return cb(err, null);
					} else {
						console.log('FLOW completed with data ', result);
            return cb(null, result);
					}
				},
				jobCallback
			);

			});
		});
	},

	init: function(redis){
    var self = this;

		var args = {
		  name: 'flowcontrol',
		  port: 0,
		  kue: {
		    prefix: 'q',
		    redis: redis
		  }
		};

		if(process.env.KUE_PORT) {
			args.ui = {
				port: process.env.KUE_PORT
			};
		}

		hose = new Hose(args);

		hose.process('run',100, function(job, done){
    	self.run(job.data.params.flowId, job.data.params.input, function(err, response){
      	if(err) console.log(err);
      	done(null, response);
    	});
		});
	}
};

function parseFlow(ngflow, cb) {
	var jobs = [];
	var tick = 0;

	_.forEach(ngflow.blocks, function(job, key){
		parseJob(job, function(parsedJob){
			next(parsedJob, key);
		});
	});

	function next(parsedJob, key){
		jobs[key] = parsedJob;
		tick++;
		if(tick===ngflow.blocks.length) {
			var flow = {
				id: ngflow.id,
				title: ngflow.title,
				blocks: jobs
			};
			if(cb){
				return cb(flow);
			} else {
				return flow;
			}
		}
	}
}


function parseJob(rawjob, next){
	var job = {
		id: rawjob.id,
		title: rawjob.title,
		service: rawjob.service,
		method: rawjob.method,
		params: rawjob.params
	};
	var params = {};
	var inputs = 0;

	_.forEach(rawjob.input, function(input, name){
		if(input){
			inputs++;
		}
	});

	_.forEach(rawjob.params, function(param,name){
		if(rawjob.input[name]) {
			if(rawjob.inputOrder[name]>= 0 && inputs>1){
				params[name] = '<INPUT['+rawjob.inputOrder[name]+']>';
			} else {
				params[name] = '<INPUT>';
			}
		} else {
			params[name] = param;
		}
	});

	job.params = params;

	if(job.service==='runner') {
		var jobs = job.params.jobs;
		job.params.jobs = [];
		_.forEach(jobs, function(flowId, flowKey){
			var j = {
				title: "VIRTUAL FLOWCONTROLER FROM MERGER",
				service: "flowcontrol",
				method: "run",
				params: {
					flowId: flowId,
					input: '<INPUT>'
				}
			};

			job.params.jobs[flowKey] = j;
		});
	}

	if(next) {
		return next(job);
	} else {
		return job;
	}
}
