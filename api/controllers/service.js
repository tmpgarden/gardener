var services = require('../../flowr');

module.exports = {
  'get /': function(req, res){
    return res.json(services);
  }
};
