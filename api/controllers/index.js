module.exports = {
  flow: require(__dirname + '/flow'),
  block: require(__dirname + '/block'),
  service: require(__dirname + '/service')
};
