var _ = require('lodash');
var Sequelize = require('sequelize');

module.exports = {
  one: function(req, res){
    // TODO: make a class method from this
    API.models.flow
    .findById(req.params.id, {
      include: [ { model: API.models.block } ],
      order: [ [ API.models.block, API.models.flowBlock, 'seq' ] ]
    })
    .then(function(item){
      return res.json(item);
    });
  },
  update: function(req, res){
    console.log(req.body);
    API.models.flow
    .findById(req.params.id).then(function(item){
      return item.update(req.body);
    })
    .then(function(item){
      item.setBlocks([])
      .then(function(){
        var promises = [];
        var order = 0;
        _.each(req.body.blocks, function(block){
          promises.push(item.addBlock(block, {seq: order++}));
        });

        return Sequelize.Promise.all(promises);
      })
      .then(function(){
        return res.json(item);
      });
    })
    .error(function(e){console.log(e);});
  },
  // test method
  'get /:id/run': function(req, res){
    API.services.flower
    .run(req.params.id, {count:5}, function(err, job){ console.log(job); } );

    return res.json({id: req.params.id});
  },

  'post /:id/run': function(req, res){
    var jobid;
    API.services.flower
    .run(
      req.params.id,
      req.body,
      function(err, result){
        console.log(result);
        API.socket.emit('result', {id: jobid, result: result});
      },
      function(job){
        jobid = job.id;
        return res.json(job.id);
      }
     );


  },
};
