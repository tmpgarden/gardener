module.exports = function(sequelize, DataTypes) {
  return sequelize.define('block', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    service: DataTypes.STRING,
    method: DataTypes.STRING,
    params: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('params');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
    info: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('info');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
    input: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('input');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
    inputOrder: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('inputOrder');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
    output: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('output');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
  }, {

  });
};
